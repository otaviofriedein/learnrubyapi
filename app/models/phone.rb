# rails g model Phone number:string contact:references

class Phone < ApplicationRecord
  belongs_to :contact, optional: true 
end