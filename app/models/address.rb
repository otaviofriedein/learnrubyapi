# rails g model Address street:string city:string contact:references
class Address < ApplicationRecord
  belongs_to :contact, optional: true 
end
