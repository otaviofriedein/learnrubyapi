# # rails g sclaffold Contact name:string email:string birthdate:date
class Contact < ApplicationRecord
    # Associations
    belongs_to :kind                                    # Required
    # belongs_to :kind, optional:true 
    has_many :phones
    has_one :address

    accepts_nested_attributes_for :phones, allow_destroy: true
    accepts_nested_attributes_for :address, update_only: true #, allow_destroy: true

    ### Usando Active Model Serializer - Tota lógica está em app/serializers/contact_serializer.rb


    # update_only: true -> Não necessita enviar 'id' de address no request, sempre será atualizado.
    # Se update_only não for especificado, será sempre criado um novo registro, porém, ao realizar o GET, sempre mostrará o ultimo (has_one)
    
    # Criando função para novo atributo 'author'
    # def author
    #     "Otávio"                                        # Return: { author: "Otávio" }
    # end

    #  # Criando função para novo atributo 'kind_description'
    # def kind_description
    #     self.kind.description                           # Return: { kind_description: description }
    # end
    
    # def to_br # Monta um JSON específico
    #     {
    #         name: self.name,
    #         email: self.email,
    #         birthdate: (I18n.l(self.birthdate) unless self.birthdate.blank?) # A menos que birthdate esteja vazia
    #     }        
    # end

    # # Sobrescreve o método as_json para os objetos Contact
    # # Toda vez que o objeto for transformado para json (render json)
    # # -> Reescreve cada atributo do JSON e/ou adiciona novos atributos
    # def as_json(options={})
    #     h = super(options)     
    #     h[:birthdate] = (I18n.l(self.birthdate) unless self.birthdate.blank?)
    #     h[:author] = self.author
    #     h[:kind_description] = self.kind_description
    #     h
    # end

    #  # -> Adiciona novos atributos através do parâmetro ':methods'
    #  def as_json(options={})
    #     super(
    #         methods: [:author, :kind_description],      # Adiciona métodos que são chamados na criação do JSON
    #         root: true,                                 # Adiciona o nome do objeto root para cada elemento do JSON 
    #         # include: :kind,                           # Adiciona o objeto relacionado 'Kind' ao JSON
    #         # include: { kind: {only: :description} }   # Mostrar somente 'description'            
    #     )           
    # end
end
