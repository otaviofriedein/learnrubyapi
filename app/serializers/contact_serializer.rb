class ContactSerializer < ActiveModel::Serializer
  attributes :id, :name, :email, :birthdate, :date_now

  belongs_to :kind do
    link(:related) {contact_kind_url(object.id) }
  end

  has_many :phones do
    link(:related) {contact_phones_url(object.id) }
  end


  has_one :address do
    link(:related) {contact_address_url(object.id) }
  end

  # link(:self) {contact_url(object.id) }
  # link(:kind) {kind_url(object.kind.id) }

  meta do
    { author: "Otávio" }
  end

  # Rewrite attributes
  def attributes(*args)
    h = super(*args)     
    h[:birthdate] = (I18n.l(object.birthdate) unless object.birthdate.blank?)    
    h[:birthdate_iso8601] = (self.birthdate_iso8601 unless object.birthdate.blank?)
    h[:kind_description] = self.kind_description
    h
  end

  def date_now
    DateTime.current.to_time.iso8601                 # Return: { date_now: "Otávio" }
  end

  def birthdate_iso8601
    object.birthdate.to_time.iso8601                 # Return: { birthdate_iso8601: "yyyy-mm-ddThh:mm:ss-UTC" }
  end

  # Criando função para novo atributo 'kind_description'
  def kind_description
    object.kind.description                           # Return: { kind_description: description }
  end


end
