class ContactsController < ApplicationController
  before_action :set_contact, only: [:show, :update, :destroy]

  # GET /contacts
  def index
    @contacts = Contact.all

    render json: @contacts 

#   render json transforma um objeto em Hash (method: as_json)
#   e em seguida transforma para String (method: to_json).

#   render json: @contacts, root: true
#   A opção 'root: true' retorna o tipo do objeto no json
#   no corpo json

#   render json: @contacts, only: [:name, :email]
#   Especifica os campos para retornar no json

#   render json: @contacts, except: [:name, :email]
#   Especifica os campos para não retornar no json

#   render json: @contacts.map { |contact| contact.attributes.merge({author: "Otávio"}) }
#   Adicionar um novo atributo na coleção de objetos json retornado

#   render json: methods: :seu_metodo
#   Permite adicionar um método personalizado, organizando o código
  end

  # GET /contacts/1
  def show
    render json: @contact, include: [:kind, :phone, :address]
    
    #   render json: @contact, include: [:kind], meta: {author: "Otávio"}
    #   Adiciona uma informação 'meta' no final do JSON

    #   render json: @contact, include: [:kind, :phones, :address]
    #   Com o Active Model Serializer, só é necessário especificar atributos 'belongs_to'

    #   render json: @contact, status: no_content 
    #   Define o status para retorno (200, 204, 401...)

    #   render json: @contact.attributes.merge({author: "Otávio"}) }
    #   Adicionar um novo atributo no objeto json retornado
  end

  # POST /contacts
  def create
    @contact = Contact.new(contact_params)

    if @contact.save
      render json: @contact, include: [:kind, :phones, :address], status: :created, location: @contact
    else
      render json: @contact.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /contacts/1
  def update
    if @contact.update(contact_params)
      render json: @contact, include: [:kind, :phones, :address]
    else
      render json: @contact.errors, status: :unprocessable_entity
    end
  end

  # DELETE /contacts/1
  def destroy
    @contact.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact
      @contact = Contact.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def contact_params
      # params.require(:contact).permit(:name, :email, :birthdate, :kind_id,
      #                                 phones_attributes: [:id, :number, :_destroy],
      #                                 address_attributes: [:id, :street, :city] #, :_destroy], # Não permite exclusão
      #                               )
      ActiveModelSerializers::Deserialization.jsonapi_parse(params)
    end
end
