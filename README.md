# README

### Configuration App

* Ruby version: 2.7.0
* Rails version: 5.x

### Start Application (Dev environment)

1. Clone this project
1. Run bundle: `bundle install`
1. Create dev database: `rails dev:setup`
1. Start application: `rails s`
1. See [here](https://www.getpostman.com/collections/bb1cde9dc5095aa5cefe) the public Postman Collection to use this API.

### Internationalization [I18n](https://guides.rubyonrails.org/i18n.html)
 
 As configurações de internacionalização encontram-se em: config/initializers/locale.rb