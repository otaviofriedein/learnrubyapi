namespace :dev do
  desc "Configura o ambiente de desenvolvimento"
  task setup: :environment do

    puts "Resetando banco de dados..."
    # Executa este comando no cmd antes do restante do código
    %x(rails db:drop db:create db:migrate)

    puts "Cadastrando tipos de contato..."

    kinds_contact = %w(Amigo Comercial Conhecido)
    kinds_contact.each do |kind|
      Kind.create!(
        description: kind
      )
    end 
    puts "Cadastro de tipos de contato realizado!"

    #######################################

    puts "Cadastrando contatos..."

    100.times do |i|
      Contact.create!(
        name: Faker::Name.name,
        email: Faker::Internet.email,
        birthdate: Faker::Date.between(65.years.ago, 18.years.ago),
        kind: Kind.all.sample
      )
    end
    puts "Cadastro de contatos realizado!"

    #######################################

    puts "Cadastrando telefones..."

    Contact.all.each do |contact|
      Random.rand(5).times do |i|
        phone = Phone.create!(number:Faker::PhoneNumber.cell_phone)
        contact.phones << phone
        contact.save!
      end 
    end    
    puts "Cadastro de telefones realizado!"

    #######################################

    puts "Cadastrando endereços..."

    Contact.all.each do |contact|      
        Address.create(
          street:Faker::Address.street_address,
          city:Faker::Address.city,
          contact: contact
        )        
    end    
    puts "Cadastro de endereços realizado!"


  end
end
